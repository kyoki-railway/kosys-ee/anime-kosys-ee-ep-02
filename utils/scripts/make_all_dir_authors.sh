#!/bin/bash

export LANG=ja_JP.UTF-8
export LC_ALL=C

pushd `dirname "$0"` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null

cd "${SCRIPTPATH}"
which cygpath > /dev/null
if [ $? -ne 0 ]; then
	REPOSITORY_ROOT="$(git rev-parse --show-toplevel)"
else
	REPOSITORY_ROOT="$(cygpath "$(git rev-parse --show-toplevel)")"
fi

cd "${REPOSITORY_ROOT}"

find . -type d  \( -path ./.git -o -path ./utils -o -path ./common -o -path ./vendor -o -path ./_site -o -path ./_release -o -path ./video/_output -o -path ./docs/_includes -o -path ./docs/_layouts -o -path ./docs/_sass -o -path ./docs/css \) -prune -o -type d -print \
	| tac \
	| \
while IFS=$'\n' read -r line; do
	echo "$line"
	pushd "${line}" > /dev/null
	
	git ls-files --error-unmatch &> /dev/null
	
	if [ $? -eq 0 ]; then
		"${SCRIPTPATH}/make_dir_authors.sh"
		git add AUTHORS.txt
	else
		echo "Skipped."
	fi
	
	popd > /dev/null
done

